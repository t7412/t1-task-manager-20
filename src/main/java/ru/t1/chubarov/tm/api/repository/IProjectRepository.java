package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    Project removeByIndex(Integer index) throws IndexIncorrectException;

}

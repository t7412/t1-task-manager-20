package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

}

package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    Task create(String userId, String name, String description) throws AbstractException;

    List<Task> findAllByProjectId(String userId, String projectId) throws AbstractException;

    Task updateById(String userId, String id, String name, String description) throws AbstractException;

    Task updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Task changeTaskStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

    Task changeTaskStatusById(String userId, String id, Status status) throws AbstractException;

}

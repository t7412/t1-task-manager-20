package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> model = findAll(userId);
        clear();
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        final List<M> result = new ArrayList<>();
        for (final M m : records) {
            if (userId.equals(m.getUserid())) result.add(m);
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty() || id == null) return null;
        for (final M model : records) {
            if (!id.equals(model.getId())) continue;
            if (!userId.equals(model.getUserid())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public int getSize(final String userId) {
        int cnt = 0;
        for (final M model : records) {
            if (userId.equals(model.getUserid())) cnt++;
        }
        return cnt;
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M add(final String userId, M model) {
        if (userId == null) return null;
        model.setUserid(userId);
        return add(model);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }


}

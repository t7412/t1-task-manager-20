package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return records.get(index);
    }

}

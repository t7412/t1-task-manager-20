package ru.t1.chubarov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException(String arg) {
        super("Error. Argument \"" + arg + "\" not support.");
    }
}

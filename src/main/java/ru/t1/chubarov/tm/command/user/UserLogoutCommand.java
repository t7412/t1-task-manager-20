package ru.t1.chubarov.tm.command.user;

import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;

public final class UserLogoutCommand extends AbstractUserCommand {

    private final String NAME = "logout";
    private final String DESCRIPTION = "User logout.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

package ru.t1.chubarov.tm.command.system;

import ru.t1.chubarov.tm.api.service.ICommandService;
import ru.t1.chubarov.tm.api.service.IProjectService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    public String getArgument() {
        return null;
    }

    public Role[] getRoles() {
        return null;
    }

}

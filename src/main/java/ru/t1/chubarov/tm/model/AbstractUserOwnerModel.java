package ru.t1.chubarov.tm.model;

public abstract class AbstractUserOwnerModel extends AbstractModel {

    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
